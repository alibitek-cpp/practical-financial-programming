#ifndef COMPOUNDINTERESTRATECALCULATOR_H
#define COMPOUNDINTERESTRATECALCULATOR_H

#include <cstdint>
#include <cmath>

class CompoundInterestRateCalculator
{
public:
    CompoundInterestRateCalculator(double interestRate);
    CompoundInterestRateCalculator(const CompoundInterestRateCalculator& other);
    CompoundInterestRateCalculator& operator=(CompoundInterestRateCalculator &other);
    ~CompoundInterestRateCalculator();

    /**
     * @brief multiplePeriod Get the future value of a fixed income investment using discrete compounding
     * We assume that interest is paid only at regular intervals, as defined by the investment vehicle
     * The compounding happens as interest is added to the original principal
     * @param value the value of the investment
     * @param numberOfPeriods the number of periods of compounding
     * @return the investment value after the specified number of periods
     */
    double multiplePeriodDiscreteCompounding(double value, uint32_t numberOfPeriods);

    /**
     * @brief multiplePeriodCountinuousCompounding Get the future value of a fixed income investment using countinuous compounding
     * We assume that compounding doesn't happen in discrete steps, but that payments are made continuously over time
     * The value returned by countinuous compounding should be slightly higher than the value achieved by discrete compounding
     * @param value the value of the investment
     * @param numberOfPeriods the number of periods of compounding
     * @return
     */
    double countinuousCompounding(double value, uint32_t numberOfPeriods);
private:
    double m_interestRate;
};

inline double CompoundInterestRateCalculator::multiplePeriodDiscreteCompounding(double value, uint32_t numberOfPeriods)
{
    return value * pow((1 + m_interestRate), numberOfPeriods);
}

inline double CompoundInterestRateCalculator::countinuousCompounding(double value, uint32_t numberOfPeriods)
{
    return value * exp(m_interestRate * numberOfPeriods);
}

#endif // COMPOUNDINTERESTRATECALCULATOR_H
