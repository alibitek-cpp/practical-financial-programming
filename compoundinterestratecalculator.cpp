#include "compoundinterestratecalculator.h"

CompoundInterestRateCalculator::CompoundInterestRateCalculator(double interestRate)
    : m_interestRate(interestRate)
{

}

CompoundInterestRateCalculator::CompoundInterestRateCalculator(const CompoundInterestRateCalculator &other)
    : m_interestRate(other.m_interestRate)
{

}

CompoundInterestRateCalculator &CompoundInterestRateCalculator::operator=(CompoundInterestRateCalculator &other)
{
    if (&other != this)
    {
        m_interestRate = other.m_interestRate;
    }

    return *this;
}

CompoundInterestRateCalculator::~CompoundInterestRateCalculator()
{

}

