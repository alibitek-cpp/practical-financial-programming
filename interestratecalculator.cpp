#include "interestratecalculator.h"

InterestRateCalculator::InterestRateCalculator(double interestRate)
    : m_interestRate(interestRate)
{

}

InterestRateCalculator::InterestRateCalculator(const InterestRateCalculator &other)
 : m_interestRate(other.m_interestRate)
{
}

InterestRateCalculator &InterestRateCalculator::operator=(const InterestRateCalculator &other)
{
    if (&other != this)
    {
        m_interestRate = other.m_interestRate;
    }

    return *this;
}

InterestRateCalculator::~InterestRateCalculator()
{

}

