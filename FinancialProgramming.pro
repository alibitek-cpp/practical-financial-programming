TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    interestratecalculator.cpp \
    compoundinterestratecalculator.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    interestratecalculator.h \
    compoundinterestratecalculator.h

QMAKE_CXXFLAGS += -Wall -Wextra -std=c++1y
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_CXXFLAGS_DEBUG += -O0 -g
