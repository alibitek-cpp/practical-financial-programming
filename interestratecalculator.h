#ifndef INTRATECALCULATOR_H
#define INTRATECALCULATOR_H


class InterestRateCalculator
{
public:
    InterestRateCalculator(double interestRate);
    InterestRateCalculator(const InterestRateCalculator& other);
    InterestRateCalculator& operator=(const InterestRateCalculator& other);
    ~InterestRateCalculator();

    /**
     * @brief singlePeriod Get the future value of a deposit after a single period
     * @param value the value of the deposit
     * @return  the future value of a deposit after a single period
     */
    double singlePeriod(double value);
private:
    double m_interestRate;
};

inline double InterestRateCalculator::singlePeriod(double value)
{
    return value * (1 + m_interestRate);
}

#endif // INTRATECALCULATOR_H
