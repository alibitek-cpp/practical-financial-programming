#include <iostream>
#include <interestratecalculator.h>
#include <compoundinterestratecalculator.h>

using namespace std;

int main(int argc, const char* argv[])
{
    if (argc != 4)
    {
        cout << "Usage: " << argv[0] << " <interest rate> <present value> <number of periods>" << endl;
    }

    double rate = atof(argv[1]);
    double value = atof(argv[2]);
    uint32_t numPeriods = stoul(argv[3]);

    InterestRateCalculator irc(rate);
    double iresult = irc.singlePeriod(value);
    cout << "[InterestRateCalculator] The future value of the deposit is: " << iresult
         << " given an interest rate of " << rate << " and the initial value of the deposit " << value << endl;

    CompoundInterestRateCalculator crc(rate);
    cout << "[CompoundInterestRateCalculator] The future value of the deposit is: " << crc.multiplePeriodDiscreteCompounding(value, numPeriods)
         << " given an interest rate of " << rate << " and the initial value of the deposit " << value
         << " for a period of " << numPeriods << " years" << endl;

    cout << "[CompoundInterestRateCalculator] The future value of the deposit is: " << crc.countinuousCompounding(value, numPeriods)
         << " given an interest rate of " << rate << " and the initial value of the deposit " << value
         << " for a period of " << numPeriods << " years" << endl;

    return 0;
}

